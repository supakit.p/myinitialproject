var express = require('express');
var app = express();
const PORT = process.env.PORT || 4200;

// parse application/json
app.use(express.json());

app.use('/', express.static('./app/'));
app.listen(PORT, () => {
    console.log(`Server is running on port : ${PORT}`)
})